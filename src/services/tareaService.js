import { api } from "@/boot/axios";

export const TareaService = {
    async get(){
        const {data} = await api.get('tasks');
        return data;
    },
    async show(id){
        const {data} = await api.get('tasks/'+id);
        return data;
    },
    async create(query){
        const {data} = await api.post('tasks',query);
        return data;
    },
    async update(id, query){
        const {data} = await api.put('tasks/'+id,query);
        return data;
    },
    async eliminar(id){
        const {data} = await api.delete('tasks/'+id,query);
        return data;
    }
};
export default { TareaService };
